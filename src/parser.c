#include "gpx2shp.h"

void charHandle(void *userdata, const XML_Char * data, int length);
void startElement(void *userdata, const char *element, const char **attr);
void endElement(void *userdata, const char *element);
void parseMain(g2sprop * prop);

/**
 * a handler to parse charctor data on expat
 */
void charHandle(void *userdata, const XML_Char * data, int length) {
	static int bufsize = DATABUFSIZE;
	parsedata *pdata = (parsedata *) userdata;
	if (bufsize < length) {
		pdata->databuf = realloc(pdata->databuf, sizeof(char) * (length + 1));
		bufsize = length;
	}
	strncpy(pdata->databuf, data, length);
	pdata->bufptr = pdata->databuf;
	pdata->bufptr += length;
	*pdata->bufptr = '\0';
}

/**
 * a handler when a element starts
 */
void startElement(void *userdata, const char *element, const char **attr) {
	parsedata *pdata = (parsedata *) userdata;
	pdata->parent = pdata->current;
	pdata->current = malloc(sizeof(parent));
	pdata->current->name = malloc(sizeof(char) * (strlen(element) + 1));
	strcpy(pdata->current->name, element);
	pdata->current->parentptr = pdata->parent;
	startElementControl(pdata, element, attr);
	if (pdata->prop->verbose) {
		int i;
		for (i = 0; i < pdata->depth; i++)
			printf("  ");
		printf("<%s>: %s", element, pdata->parent->name);
		for (i = 0; attr[i]; i += 2) {
			printf(" %s='%s'", attr[i], attr[i + 1]);
		}
		printf("\n");
	}
	pdata->depth++;
}

/**
 * a handler when a element ends
 */
void endElement(void *userdata, const char *element) {
	parsedata *pdata = (parsedata *) userdata;
	endElementControl(pdata, element);
	if (pdata->prop->verbose) {
		int i;
		if (!strcmp(element, "name")) {
			printf("%s\n", pdata->databuf);
		}
		for (i = 0; i < pdata->depth; i++)
			printf("  ");
		printf("</%s>\n", pdata->current->name);
	}
	free(pdata->current->name);
	free(pdata->current);
	pdata->current = pdata->parent;
	pdata->parent = pdata->parent->parentptr;
	pdata->depth--;
}

void parseMain(g2sprop * prop) {
	FILE *fp;
	char buff[BUFFSIZE];
	XML_Parser parser;
	parsedata *pdata;
	fp = fopen(prop->sourcefile, "r");
	if (fp == NULL ) {
		fprintf(stderr, "Cannot open gpx file: %s\n", prop->sourcefile);
		exit(ERR_CANNOTOPEN);
	}
	parser = XML_ParserCreate(NULL);
	if (!parser) {
		fprintf(stderr, "Couldn't allocate memory for parser\n");
		exit(ERR_OUTOFMEMORY);
	}
	pdata = createParsedata(parser, prop);
	XML_SetUserData(parser, pdata);
	XML_SetElementHandler(parser, startElement, endElement);
	XML_SetCharacterDataHandler(parser, charHandle);
	for (;;) {
		char* result = fgets(buff, (int)BUFFSIZE, fp);
		int len = (int)(strlen(buff));
		if (ferror(fp)) {
			fprintf(stderr, "Read error file: %s\n", prop->sourcefile);
			exit(ERR_READERROR);
		}
		if ( result == NULL ) {
			break;
		}
		if (!XML_Parse(parser, buff, len, 0)) {
			fprintf(stderr, "Parse error at line %d:\n%s\n",
					(int)(XML_GetCurrentLineNumber(parser)),
					XML_ErrorString(XML_GetErrorCode(parser)));
			exit(ERR_PARSEERROR);
		}
	}
	closeShpFiles(pdata->shps);
	closeDbfFiles(pdata->dbfs);
	closeParsedata(pdata);
}
