#include "gpx2shp.h"

void startElementControl(parsedata * pdata, const char *element,
		const char **attr);
void endElementControl(parsedata * pdata, const char *element);

/*
 * This method controls tag start event.
 * It corrects attributes.
 */
void startElementControl(parsedata * pdata, const char *element,
		const char **attr) {
	int i;
	static int isFirstTrk = 1;
	static int isFirstRte = 1;
	static int isFirstPathpt = 1;
	for (i = 0; attr[i]; i += 2) {
		if (!strcmp(attr[i], "lon")) {
			pdata->attr->lon = atof(attr[i + 1]);
		}
		if (!strcmp(attr[i], "lat")) {
			pdata->attr->lat = atof(attr[i + 1]);
		}
		if (!strcmp(attr[i], "minlon")) {
			pdata->attr->minlon = atof(attr[i + 1]);
		}
		if (!strcmp(attr[i], "minlat")) {
			pdata->attr->minlat = atof(attr[i + 1]);
		}
		if (!strcmp(attr[i], "maxlon")) {
			pdata->attr->maxlon = atof(attr[i + 1]);
		}
		if (!strcmp(attr[i], "maxlat")) {
			pdata->attr->maxlat = atof(attr[i + 1]);
		}
		if (!strcmp(attr[i], "author")) {
			strncpy(pdata->attr->author, attr[i + 1], NAMELENGTH);
		}
	}
	if (pdata->prop->parseTrk) {
		if (!strcmp(element, "trk")) {
			if (isFirstTrk) {
				initPath(pdata);
				isFirstTrk = 0;
			}
		}
		if (!strcmp(element, "trkseg")) {
			isFirstPathpt = 1;
		}
		if (!strcmp(element, "trkpt")) {
			if (isFirstPathpt) {
				initPathAttr(pdata->pattr, pdata->attr);
				isFirstPathpt = 0;
			}
		}
	}
	if (pdata->prop->parseRte) {
		if (!strcmp(element, "rte")) {
			if (isFirstRte) {
				initPath(pdata);
				isFirstRte = 0;
				isFirstPathpt = 1;
			}
		}
		if (!strcmp(element, "rtept")) {
			if (isFirstPathpt) {
				initPathAttr(pdata->pattr, pdata->attr);
				isFirstPathpt = 0;
			}
		}
	}
}

/**
 * This method is kicked by tag end event.
 * It corrects char elements when the element tag has some data,
 * then start to convert when tag is top level tag like <wpt>.
 */
void endElementControl(parsedata * pdata, const char *element) {
	static int isFirstWpt = 1;
	static int isFirstTrkAsPoint = 1;
	static int isFirstRteAsPoint = 1;
	/* common elements */
	if (!strcmp(element, "name")) {
		strncpy(pdata->attr->name, pdata->databuf, NAMELENGTH);
	}
	if (!strcmp(element, "cmt")) {
		strncpy(pdata->attr->cmt, pdata->databuf, COMMENTLENGTH);
	}
	if (!strcmp(element, "desc")) {
		strncpy(pdata->attr->desc, pdata->databuf, COMMENTLENGTH);
	}
	if (!strcmp(element, "src")) {
		strncpy(pdata->attr->src, pdata->databuf, COMMENTLENGTH);
	}
	if (!strcmp(element, "link")) {
		strncpy(pdata->attr->link, pdata->databuf, FILENAMELENGTH);
	}
	if (!strcmp(element, "type")) {
		strncpy(pdata->attr->type, pdata->databuf, TYPELENGTH);
	}
	/* waypoint and metadata elements */
	if (!strcmp(element, "time")) {
		strncpy(pdata->attr->time, pdata->databuf, TIMELENGTH);
	}
	/* route and track point elements */
	if (!strcmp(element, "number")) {
		pdata->attr->number = atoi(pdata->databuf);
	}
	/* waypoint elements */
	if (!strcmp(element, "ele")) {
		pdata->attr->ele = atof(pdata->databuf);
	}
	if (!strcmp(element, "magvar")) {
		pdata->attr->magvar = atof(pdata->databuf);
	}
	if (!strcmp(element, "geoidheight")) {
		pdata->attr->geoidheight = atof(pdata->databuf);
	}
	if (!strcmp(element, "sym")) {
		strncpy(pdata->attr->sym, pdata->databuf, NAMELENGTH);
	}
	if (!strcmp(element, "fix")) {
		strncpy(pdata->attr->fix, pdata->databuf, NAMELENGTH);
	}
	if (!strcmp(element, "sat")) {
		pdata->attr->sat = atoi(pdata->databuf);
	}
	if (!strcmp(element, "hdop")) {
		pdata->attr->hdop = atof(pdata->databuf);
	}
	if (!strcmp(element, "vdop")) {
		pdata->attr->vdop = atof(pdata->databuf);
	}
	if (!strcmp(element, "pdop")) {
		pdata->attr->pdop = atof(pdata->databuf);
	}
	if (!strcmp(element, "ageofdgpsdata")) {
		pdata->attr->ageofdgpsdata = atof(pdata->databuf);
	}
	/* metadata elements */
	if (!strcmp(element, "author")) {
		strncpy(pdata->attr->author, pdata->databuf, NAMELENGTH);
	}
	if (!strcmp(element, "keywords")) {
		strncpy(pdata->attr->keywords, pdata->databuf, NAMELENGTH);
	}
	if (!strcmp(element, "copyright")) {
		strncpy(pdata->attr->copyright, pdata->databuf, NAMELENGTH);
	}
	if (!strcmp(element, "year")) {
		pdata->attr->year = atoi(pdata->databuf);
	}
	if (!strcmp(element, "license")) {
		strncpy(pdata->attr->license, pdata->databuf, NAMELENGTH);
	}
	if (!strcmp(element, "bounds")) {
		/* none */
	}
	/* top elements */
	/* set waypoint data */
	if (!strcmp(element, "wpt")) {
		if (pdata->prop->parseWpt) {
			if (isFirstWpt) {
				initWpt(pdata);
				isFirstWpt = 0;
			}
			setWpt(pdata->shps->wpt, pdata->dbfs->wpt, pdata);
			wipeAttr(pdata->attr);
		}
	}
	/* set trackpoint data */
	if (!strcmp(element, "trkpt")) {
		if (pdata->prop->parseTrk) {
			setPathData(pdata->pattr, pdata->attr);
			if (!pdata->prop->isFast)
				setPathInterval(pdata);
		}
		/* set trackpoint data as point */
		if (pdata->prop->isPoint) {
			if (isFirstTrkAsPoint) {
				initPathAsPoint(pdata);
				isFirstTrkAsPoint = 0;
			}
			setWpt(pdata->shps->trk_pnt, pdata->dbfs->trk_pnt, pdata);
		}
		wipeAttr(pdata->attr);
	}
	/* write trackpoint */
	if (!strcmp(element, "trkseg")) {
		if (pdata->prop->parseTrk) {
			setPath(pdata->shps->trk, pdata->dbfs->trk, pdata);
		}
	}
	/* set route data */
	if (!strcmp(element, "rtept")) {
		if (pdata->prop->parseRte) {
			setPathData(pdata->pattr, pdata->attr);
			if (!pdata->prop->isFast)
				setPathInterval(pdata);
		}
		/* set route data as point */
		if (pdata->prop->isPoint) {
			if (isFirstRteAsPoint) {
				initPathAsPoint(pdata);
				isFirstRteAsPoint = 0;
			}
			setWpt(pdata->shps->rte_pnt, pdata->dbfs->rte_pnt, pdata);
		}
		wipeAttr(pdata->attr);
	}
	/* write route */
	if (!strcmp(element, "rte")) {
		if (pdata->prop->parseRte) {
			setPath(pdata->shps->rte, pdata->dbfs->rte, pdata);
		}
	}
	if (!strcmp(element, "metadata")) {
		setMetadata(pdata);
		wipeAttr(pdata->attr);
	}
}
