#include "gpx2shp.h"

void writeWptAttribute(DBFHandle hDBF, parsedata * pdata, int iShape);
void setWpt(SHPHandle hSHP, DBFHandle hDBF, parsedata * pdata);
void addWptField(DBFHandle hDBF, g2scolumns * cols);
void initWpt(parsedata * pdata);

/**
 * save point attributes in *_wpt.dbf or *_pnt.dbf
 */
void writeWptAttribute(DBFHandle hDBF, parsedata * pdata, int iShape) {
	g2scolumns *cols = pdata->prop->cols;
	g2sattr *attr = pdata->attr;
	int c = 1;
	if (!DBFWriteIntegerAttribute(hDBF, iShape, 0, iShape)) {
		failToWriteAttr(iShape, 0);
	}
	if (cols->ele) {
		if (!DBFWriteDoubleAttribute(hDBF, iShape, c, attr->ele))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->time) {
		if (!DBFWriteStringAttribute(hDBF, iShape, c, attr->time))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->magvar) {
		if (!DBFWriteDoubleAttribute(hDBF, iShape, c, attr->magvar))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->geoidheight) {
		if (!DBFWriteDoubleAttribute(hDBF, iShape, c, attr->geoidheight))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->name) {
		if (!DBFWriteStringAttribute(hDBF, iShape, c, attr->name))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->cmt) {
		if (!DBFWriteStringAttribute(hDBF, iShape, c, attr->cmt))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->desc) {
		if (!DBFWriteStringAttribute(hDBF, iShape, c, attr->desc))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->src) {
		if (!DBFWriteStringAttribute(hDBF, iShape, c, attr->src))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->link) {
		if (!DBFWriteStringAttribute(hDBF, iShape, c, attr->link))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->sym) {
		if (!DBFWriteStringAttribute(hDBF, iShape, c, attr->sym))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->type) {
		if (!DBFWriteStringAttribute(hDBF, iShape, c, attr->type))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->sat) {
		if (!DBFWriteIntegerAttribute(hDBF, iShape, c, attr->sat))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->hdop) {
		if (!DBFWriteDoubleAttribute(hDBF, iShape, c, attr->hdop))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->vdop) {
		if (!DBFWriteDoubleAttribute(hDBF, iShape, c, attr->vdop))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->ageofdgpsdata) {
		if (!DBFWriteDoubleAttribute(hDBF, iShape, c, attr->ageofdgpsdata))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->dgpsid) {
		if (!DBFWriteIntegerAttribute(hDBF, iShape, c, attr->dgpsid))
			failToWriteAttr(iShape, c);
	}
}

/**
 * save point objects in *_wpt or *_pnt file
 */
void setWpt(SHPHandle hSHP, DBFHandle hDBF, parsedata * pdata) {
	int iShape = 1;
	SHPObject *shpobj;
	double x[1], y[1], z[1];
	x[0] = pdata->attr->lon;
	y[0] = pdata->attr->lat;
	z[0] = pdata->attr->ele;
	if (pdata->prop->is3d) {
		shpobj = SHPCreateObject(SHPT_POINTZ, iShape, 0, 0, 0, 1, x, y, z,
				NULL );
	} else {
		shpobj = SHPCreateObject(SHPT_POINT, iShape, 0, 0, 0, 1, x, y, NULL,
				NULL );
	}
	iShape = SHPWriteObject(hSHP, -1, shpobj);
	SHPDestroyObject(shpobj);
	writeWptAttribute(hDBF, pdata, iShape);
	if (!strcmp(pdata->current->name, "wpt")) {
		pdata->prop->stats->wptpoints++;
	}
	return;
}

/**
 * set attribute column of point data
 */
void addWptField(DBFHandle hDBF, g2scolumns * cols) {
	DBFAddField(hDBF, "shapeID", FTInteger, 8, 0);
	if (cols->ele)
		DBFAddField(hDBF, "ele", FTDouble, 16, 4);
	if (cols->time)
		DBFAddField(hDBF, "time", FTString, TIMELENGTH, 0);
	if (cols->magvar)
		DBFAddField(hDBF, "magvar", FTDouble, 16, 4);
	if (cols->geoidheight)
		DBFAddField(hDBF, "geoidheight", FTDouble, 16, 4);
	if (cols->name)
		DBFAddField(hDBF, "name", FTString, NAMELENGTH, 0);
	if (cols->cmt)
		DBFAddField(hDBF, "cmt", FTString, COMMENTLENGTH, 0);
	if (cols->desc)
		DBFAddField(hDBF, "desc", FTString, COMMENTLENGTH, 0);
	if (cols->src)
		DBFAddField(hDBF, "src", FTString, COMMENTLENGTH, 0);
	if (cols->link)
		DBFAddField(hDBF, "link", FTString, FILENAMELENGTH, 0);
	if (cols->sym)
		DBFAddField(hDBF, "sym", FTString, NAMELENGTH, 0);
	if (cols->type)
		DBFAddField(hDBF, "type", FTString, NAMELENGTH, 0);
	if (cols->sat)
		DBFAddField(hDBF, "sat", FTInteger, 8, 0);
	if (cols->hdop)
		DBFAddField(hDBF, "hdop", FTDouble, 16, 4);
	if (cols->vdop)
		DBFAddField(hDBF, "vdop", FTDouble, 16, 4);
	if (cols->ageofdgpsdata)
		DBFAddField(hDBF, "ageofdgpsdata", FTDouble, 16, 4);
	if (cols->dgpsid)
		DBFAddField(hDBF, "dgpsid", FTDouble, 16, 4);
}

/**
 * Creates *_wpt.shp and *_wpt.dbf file
 */
void initWpt(parsedata * pdata) {
	char *output_wpt = (char *) malloc(
			sizeof(char) * (strlen(pdata->prop->output) + 5));
	strcpy(output_wpt, pdata->prop->output);
	strcat(output_wpt, "_wpt");
	if (pdata->prop->is3d) {
		pdata->shps->wpt = SHPCreate(output_wpt, SHPT_POINTZ);
	} else {
		pdata->shps->wpt = SHPCreate(output_wpt, SHPT_POINT);
	}
	pdata->dbfs->wpt = DBFCreate(output_wpt);
	addWptField(pdata->dbfs->wpt, pdata->prop->cols);
	free(output_wpt);
}
