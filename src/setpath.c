#include "gpx2shp.h"

void writePathAttribute(DBFHandle hDBF, parsedata * pdata, pathattr * pattr,
		int iShape);
void addPathField(DBFHandle hDBF, parsedata * pdata);
void initPathAttr(pathattr * pattr, g2sattr * attr);
void initPath(parsedata * pdata);
void initPathAsPoint(parsedata * pdata);
void initPathAsEdge(parsedata * pdata);
void setEdge(parsedata * pdata, double _x, double _y, double _z, double length,
		double interval, double speed);
void setPathInterval(parsedata * pdata);
void setPathData(pathattr * pattr, g2sattr * attr);
void countUnconverted(parsedata * pdata);
void countPath(parsedata * pdata);
int checkPath(parsedata * pdata);
void setPath(SHPHandle hSHP, DBFHandle hDBF, parsedata * pdata);

/**
 * set each shape attribute data
 */
void writePathAttribute(DBFHandle hDBF, parsedata * pdata, pathattr * pattr,
		int iShape) {
	g2sprop *prop = pdata->prop;
	g2scolumns *cols = prop->cols;
	int c = 1;
	if (!DBFWriteIntegerAttribute(hDBF, iShape, 0, iShape)) {
		failToWriteAttr(iShape, 0);
	}
	if (cols->name) {
		if (!DBFWriteStringAttribute(hDBF, iShape, c, pattr->name))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->cmt) {
		if (!DBFWriteStringAttribute(hDBF, iShape, c, pattr->cmt))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->desc) {
		if (!DBFWriteStringAttribute(hDBF, iShape, c, pattr->desc))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->src) {
		if (!DBFWriteStringAttribute(hDBF, iShape, c, pattr->src))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->link) {
		if (!DBFWriteStringAttribute(hDBF, iShape, c, pattr->link))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->type) {
		if (!DBFWriteStringAttribute(hDBF, iShape, c, pattr->type))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->length) {
		if (!DBFWriteDoubleAttribute(hDBF, iShape, c,
				(pattr->length / prop->length2meter)))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->interval) {
		if (!DBFWriteDoubleAttribute(hDBF, iShape, c,
				(pattr->interval / prop->time2sec))) {
			printf("%f, %f, %f\n", pattr->interval, prop->time2sec,
					(pattr->interval / prop->time2sec));
			failToWriteAttr(iShape, c);
		}
		c++;
	}
	if (cols->speed) {
		if (!DBFWriteDoubleAttribute(hDBF, iShape, c, pattr->speed))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->points) {
		if (!DBFWriteIntegerAttribute(hDBF, iShape, c, pattr->count))
			failToWriteAttr(iShape, c);
		c++;
	}
	if (cols->gpxline) {
		if (!DBFWriteIntegerAttribute(hDBF, iShape, c,
				XML_GetCurrentLineNumber(pdata->parser)))
			failToWriteAttr(iShape, c);
	}
}

/**
 * add a shape attribute table columns
 */
void addPathField(DBFHandle hDBF, parsedata * pdata) {
	char columnName[16];
	g2sprop *prop = pdata->prop;
	g2scolumns *cols = prop->cols;
	DBFAddField(hDBF, "shapeID", FTInteger, 8, 0);
	if (cols->name)
		DBFAddField(hDBF, "name", FTString, NAMELENGTH, 0);
	if (cols->cmt)
		DBFAddField(hDBF, "cmt", FTString, COMMENTLENGTH, 0);
	if (cols->desc)
		DBFAddField(hDBF, "desc", FTString, COMMENTLENGTH, 0);
	if (cols->src)
		DBFAddField(hDBF, "src", FTString, COMMENTLENGTH, 0);
	if (cols->link)
		DBFAddField(hDBF, "link", FTString, FILENAMELENGTH, 0);
	if (cols->type)
		DBFAddField(hDBF, "type", FTString, NAMELENGTH, 0);
	if (cols->length) {
		sprintf(columnName, "l(%s)", prop->lengthUnit);
		DBFAddField(hDBF, columnName, FTDouble, 17, 4);
	}
	if (cols->time) {
		sprintf(columnName, "t(%s)", prop->timeUnit);
		DBFAddField(hDBF, columnName, FTDouble, 12, 2);
	}
	if (cols->speed) {
		sprintf(columnName, "s(%s/%s)", prop->speedLengthUnit,
				prop->speedTimeUnit);
		DBFAddField(hDBF, columnName, FTDouble, 12, 2);
	}
	if (cols->points) {
		DBFAddField(hDBF, "points", FTInteger, 12, 0);
	}
	if (cols->gpxline) {
		DBFAddField(hDBF, "gpxline#", FTInteger, 12, 0);
	}
}

/**
 * initialize a path attribute
 */
void initPathAttr(pathattr * pattr, g2sattr * attr) {
	strncpy(pattr->name, attr->name, NAMELENGTH);
	strncpy(pattr->cmt, attr->cmt, COMMENTLENGTH);
	strncpy(pattr->desc, attr->desc, COMMENTLENGTH);
	strncpy(pattr->src, attr->src, COMMENTLENGTH);
	strncpy(pattr->link, attr->link, FILENAMELENGTH);
	pattr->number = attr->number;
	strncpy(pattr->type, attr->type, TYPELENGTH);
	pattr->length = 0;
	pattr->interval = 0;
	pattr->speed = 0;
	pattr->count = 0;
	pattr->point = NULL;
}

/**
 * creates *_[trk|rte].shp and *_[trk|rte].dbf file
 */
void initPath(parsedata * pdata) {
	SHPHandle shp;
	DBFHandle dbf;
	char *output = (char *) malloc(
			sizeof(char) * (strlen(pdata->prop->output) + 5));
	strcpy(output, pdata->prop->output);
	if (!strcmp(pdata->current->name, "trk")) {
		strcat(output, "_trk");
	} else {
		strcat(output, "_rte");
	}
	if (pdata->prop->is3d) {
		shp = SHPCreate(output, SHPT_ARCZ);
	} else {
		shp = SHPCreate(output, SHPT_ARC);
	}
	dbf = DBFCreate(output);
	if (!strcmp(pdata->current->name, "trk")) {
		pdata->shps->trk = shp;
		pdata->dbfs->trk = dbf;
	} else {
		pdata->shps->rte = shp;
		pdata->dbfs->rte = dbf;
	}
	addPathField(dbf, pdata);
	free(output);
}

/**
 * creates *_[trk|rte]_pnt.shp and *_[trk|rte]_pnt.dbf file
 */
void initPathAsPoint(parsedata * pdata) {
	SHPHandle shp;
	DBFHandle dbf;
	char *output = (char *) malloc(
			sizeof(char) * (strlen(pdata->prop->output) + 9));
	strcpy(output, pdata->prop->output);
	if (!strcmp(pdata->current->name, "trkpt")) {
		strcat(output, "_trk_pnt");
	} else {
		strcat(output, "_rte_pnt");
	}
	if (pdata->prop->is3d) {
		shp = SHPCreate(output, SHPT_POINTZ);
	} else {
		shp = SHPCreate(output, SHPT_POINT);
	}
	dbf = DBFCreate(output);
	addWptField(dbf, pdata->prop->cols);
	if (!strcmp(pdata->current->name, "trkpt")) {
		pdata->shps->trk_pnt = shp;
		pdata->dbfs->trk_pnt = dbf;
	} else {
		pdata->shps->rte_pnt = shp;
		pdata->dbfs->rte_pnt = dbf;
	}
	free(output);
}

/**
 * creates *_[trk|rte]_edg.shp and *_[trk|rte]_edg.dbf file
 */
void initPathAsEdge(parsedata * pdata) {
	SHPHandle shp;
	DBFHandle dbf;
	char *output = (char *) malloc(
			sizeof(char) * (strlen(pdata->prop->output) + 9));
	strcpy(output, pdata->prop->output);
	if (!strcmp(pdata->current->name, "trkpt")) {
		strcat(output, "_trk_edg");
	} else {
		strcat(output, "_rte_edg");
	}
	if (pdata->prop->is3d) {
		shp = SHPCreate(output, SHPT_ARCZ);
	} else {
		shp = SHPCreate(output, SHPT_ARC);
	}
	dbf = DBFCreate(output);
	addPathField(dbf, pdata);
	if (!strcmp(pdata->current->name, "trkpt")) {
		pdata->shps->trk_edg = shp;
		pdata->dbfs->trk_edg = dbf;
	} else {
		pdata->shps->rte_edg = shp;
		pdata->dbfs->rte_edg = dbf;
	}
	free(output);
}

/**
 * set edge data and store it
 */
void setEdge(parsedata * pdata, double _x, double _y, double _z, double length,
		double interval, double speed) {
	SHPHandle shp;
	DBFHandle dbf;
	SHPObject *shpobj;
	pathattr *pattr = pdata->pattr;
	static int iShape = 0;
	static int isFirstTrkAsEdge = 1;
	static int isFirstRteAsEdge = 1;
	double x[2], y[2], z[2];
	double _length, _interval, _speed;
	if (!strcmp(pdata->current->name, "trkpt")) {
		if (isFirstTrkAsEdge) {
			initPathAsEdge(pdata);
			isFirstTrkAsEdge = 0;
		}
		shp = pdata->shps->trk_edg;
		dbf = pdata->dbfs->trk_edg;
	} else {
		if (isFirstRteAsEdge) {
			initPathAsEdge(pdata);
			isFirstRteAsEdge = 0;
		}
		shp = pdata->shps->rte_edg;
		dbf = pdata->dbfs->rte_edg;
	}
	_length = pattr->length;
	_interval = pattr->interval;
	_speed = pattr->speed;
	pattr->length = length;
	pattr->interval = interval;
	pattr->speed = speed;
	x[0] = _x;
	y[0] = _y;
	z[0] = _z;
	x[1] = pdata->attr->lon;
	y[1] = pdata->attr->lat;
	z[1] = pdata->attr->ele;
	if (pdata->prop->is3d) {
		shpobj = SHPCreateObject(SHPT_ARCZ, iShape, 0, 0, 0, 2, x, y, z, NULL );
	} else {
		shpobj = SHPCreateObject(SHPT_ARC, iShape, 0, 0, 0, 2, x, y, NULL,
				NULL );
	}
	iShape = SHPWriteObject(shp, -1, shpobj);
	SHPDestroyObject(shpobj);
	writePathAttribute(dbf, pdata, pattr, iShape);
	pattr->length = _length;
	pattr->interval = _interval;
	pattr->speed = _speed;
}

/**
 * sets interval data between two track points
 */
void setPathInterval(parsedata * pdata) {
	pathattr *pattr = pdata->pattr;
	g2sattr *attr = pdata->attr;
	g2sprop *prop = pdata->prop;
	static char _t[TIMELENGTH];
	double intvl = 0;
	static double _x, _y, _z;
	double leng = 0;
	double spd;
	if (pattr->count == 1) {
		strncpy(_t, attr->time, TIMELENGTH);
	} else {
		/* time interval */
		intvl = getTimeInterval(_t, attr->time);
		pattr->interval = pattr->interval + intvl;
		strncpy(_t, attr->time, TIMELENGTH);
		/* length interval */
		leng = getDistance(_x, _y, attr->lon, attr->lat);
		pattr->length = pattr->length + leng;
		/* interval speed */
		spd = getSpeed(leng, intvl, prop->speed2meter, prop->speed2sec);
		/* sets edge data */
		if (prop->isEdge) {
			setEdge(pdata, _x, _y, _z, leng, intvl, spd);
		}
	}
	_x = attr->lon;
	_y = attr->lat;
	_z = attr->ele;
}

/**
 * sets each track point data in array.
 */
void setPathData(pathattr * pattr, g2sattr * attr) {
	const int reallocsize = 100;
	if (pattr->count == 0) {
		pattr->point = malloc(sizeof(double) * 3 * reallocsize);
	}
	if ((pattr->count % reallocsize) == 0) {
		pattr->point = realloc(pattr->point,
				sizeof(double) * 3 * (pattr->count + reallocsize));
	}
	pattr->point[pattr->count * 3] = attr->lon;
	pattr->point[pattr->count * 3 + 1] = attr->lat;
	pattr->point[pattr->count * 3 + 2] = attr->ele;
	pattr->count++;
}

/**
 * counts paths that wasn't converted
 */
void countUnconverted(parsedata * pdata) {
	g2sstats *stats = pdata->prop->stats;
	if (!strcmp(pdata->current->name, "trkseg"))
		stats->trkunconverted++;
	else
		stats->rteunconverted++;
}

/**
 * counts paths 
 */
void countPath(parsedata * pdata) {
	g2sstats *stats = pdata->prop->stats;
	pathattr *pattr = pdata->pattr;
	if (!strcmp(pdata->current->name, "trkseg")) {
		stats->trkcount++;
		stats->trklength += pattr->length;
		stats->trkpoints += pattr->count;
	} else {
		stats->rtecount++;
		stats->rtelength += pattr->length;
		stats->rtepoints += pattr->count;
	}
}

int checkPath(parsedata * pdata) {
	pathattr *pattr = pdata->pattr;
	g2sprop *prop = pdata->prop;
	/* check point count. */
	if (pattr->count < prop->minpoints) {
		fprintf(stderr,
				"gpx2shp:%s:%i track was not converted because of less then %d points. \n",
				prop->sourcefile, (int)XML_GetCurrentLineNumber(pdata->parser),
				prop->minpoints);
		countUnconverted(pdata);
		return 0;
		/* check path length */
	} else if (pattr->length < prop->minlength * prop->length2meter) {
		fprintf(stderr,
				"gpx2shp:%s:%i track was not converted because it is shorter than %dm.\n",
				prop->sourcefile, (int)XML_GetCurrentLineNumber(pdata->parser),
				prop->minlength);
		countUnconverted(pdata);
		return 0;
		/* check path time */
	} else if (pattr->interval < prop->mintime * prop->time2sec) {
		fprintf(stderr,
				"gpx2shp:%s:%i track was not converted because it is shorter than %d sed.\n",
				prop->sourcefile, (int)XML_GetCurrentLineNumber(pdata->parser),
				prop->mintime);
		countUnconverted(pdata);
		return 0;
		/* check path speed */
	} else if (0 && pattr->speed == .0) {
		fprintf(stderr,
				"gpx2shp:%s:%i track was not converted because no move recorded.\n",
				prop->sourcefile, (int)XML_GetCurrentLineNumber(pdata->parser));
		countUnconverted(pdata);
		return 0;
	}
	return 1;
}

/**
 * saves path data into files.
 */
void setPath(SHPHandle hSHP, DBFHandle hDBF, parsedata * pdata) {
	SHPObject *shpobj;
	static int iShape = 0;
	pathattr *pattr = pdata->pattr;
	g2sprop *prop = pdata->prop;
	int isOk = 0;
	pattr->speed = getSpeed(pattr->length, pattr->interval, prop->speed2meter,
			prop->speed2sec);
	if (prop->isFast) {
		isOk = 1;
	} else {
		isOk = checkPath(pdata);
	}
	if (isOk) {
		double x[pattr->count];
		double y[pattr->count];
		double z[pattr->count];
		int i;
		for (i = 0; i < pattr->count; i++) {
			x[i] = pattr->point[i * 3];
			y[i] = pattr->point[i * 3 + 1];
			z[i] = pattr->point[i * 3 + 2];
		}
		if (pdata->prop->is3d) {
			shpobj = SHPCreateObject(SHPT_ARCZ, iShape, 0, 0, 0, pattr->count,
					x, y, z, NULL );
		} else {
			shpobj = SHPCreateObject(SHPT_ARC, iShape, 0, 0, 0, pattr->count, x,
					y, NULL, NULL );
		}
		iShape = SHPWriteObject(hSHP, -1, shpobj);
		SHPDestroyObject(shpobj);
		countPath(pdata);
		writePathAttribute(hDBF, pdata, pattr, iShape);
	}

}
